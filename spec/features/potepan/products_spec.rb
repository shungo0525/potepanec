require 'rails_helper'

RSpec.feature "ProductsSystems", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let(:product)  { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  describe '商品詳細ページ' do
    before do
      visit potepan_product_path(product.id)
    end

    it "対象商品のデータが表示される" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    it 'Homeリンクが正常に動作する' do
      click_link "Home", match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it "商品詳細ページから関連したカテゴリーの一覧ページへ移動する" do
      expect(page).to have_link '一覧ページへ戻る'
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "関連商品のデータが表示される" do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).to have_content related_product.description
    end

    it "関連商品から商品詳細ページに移動する" do
      expect(page).to have_link related_product.name
      click_on related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).to have_content related_product.description
    end
  end
end
