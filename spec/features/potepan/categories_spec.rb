require 'rails_helper'

RSpec.feature "CategoriesSystems", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe 'カテゴリーページ' do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "カテゴリーページに対象のデータが表示されている" do
      expect(page).to have_content taxon.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end

    it "正常にサイドバーが表示される" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content "#{taxon.name} (#{taxon.products.count})"
        click_on "#{taxon.name} (#{taxon.products.count})"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    it '商品名から商品詳細へのリンクが機能している' do
      expect(page).to have_link product.name
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
