require 'rails_helper'

RSpec.describe "ProductsRequests", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let(:product)  { create(:product, taxons: [taxon]) }
  let!(:related_products)     { create_list(:product, 5, taxons: [taxon]) }
  let!(:not_related_product)  { create(:product) }

  describe "商品詳細ページへのアクセス" do
    before do
      get potepan_product_path(product.id)
    end

    it "正常なレスポンスを返す" do
      expect(response).to have_http_status(:ok)
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template(:show)
    end

    it "@productが期待される値を持つ" do
      expect(response.body).to include(product.name)
    end

    it "@related_productsが期待される値を持つ" do
      expect(response.body).to include(related_products[0].name)
    end

    it "関連しない商品は表示しない" do
      expect(response.body).not_to include(not_related_product.name)
    end

    it "関連商品内でメインの商品は表示しない" do
      expect(related_products).not_to include(product.name)
    end

    it "関連商品を5個作成して、4件のみ表示する" do
      expect(response.body).to include(related_products[0].name)
      expect(response.body).to include(related_products[1].name)
      expect(response.body).to include(related_products[2].name)
      expect(response.body).to include(related_products[3].name)
      expect(response.body).not_to include(related_products[4].name)
    end
  end
end
