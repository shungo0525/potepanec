require 'rails_helper'

RSpec.describe "CategoriesRequests", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "カテゴリーページへのアクセス" do
    before do
      get potepan_category_path(taxon.id)
    end

    it "正常なレスポンスを返す" do
      expect(response).to have_http_status(:ok)
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template(:show)
    end

    it 'taxonomyが値を取得できている' do
      expect(response.body).to include(taxonomy.name)
    end

    it 'taxonが値を取得できている' do
      expect(response.body).to include(taxon.name)
    end

    it 'productが値を取得できている' do
      expect(response.body).to include(product.name)
    end
  end
end
