require 'rails_helper'

RSpec.describe "ApplicationHelper", type: :helper do
  include ApplicationHelper

  describe "商品詳細ページへのアクセス" do
    context "タイトルの引数がある場合" do
      it "タイトルが正しく表示される" do
        expect(helper.full_title("T-shirts")).to eq("T-shirts - BIGBAG Store")
      end
    end

    context "タイトルの引数がない場合" do
      it "タイトルが正しく表示される" do
        expect(helper.full_title("")).to eq("BIGBAG Store")
      end
    end
  end
end
