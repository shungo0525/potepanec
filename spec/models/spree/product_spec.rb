require 'rails_helper'
RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let(:taxonomy) { create :taxonomy }
    let(:taxon) { create :taxon, taxonomy: taxonomy }
    let(:product) { create :product, taxons: [taxon] }
    let(:related_product) { create :product, taxons: [taxon] }
    let(:other_taxon) { create :taxon }
    let(:not_related_product) { create :product, taxons: [other_taxon] }

    it "関連商品を持つ" do
      expect(product.related_products).to include related_product
    end

    it "関連しない商品を持たない" do
      expect(product.related_products).not_to include not_related_product
    end
  end
end
